public class Pokemon {

    //Pokemon Attributes
    private String pokeName;
    private int pokeGrade;
    private String moveType;
    private int pokePE;
    private String zMove;
    private int statHP,statSPA,statDEF,statSPD,statSPEED;

    //Pokemon Constructor
    public Pokemon(String pokeName, int pokeGrade, String moveType, int pokePE,
            String zMove, int statHP, int statSPA, int statDEF, int statSPD, int statSPEED) {
        this.pokeName = pokeName;
        this.pokeGrade = pokeGrade;
        this.moveType = moveType;
        this.pokePE = pokePE;
        this.zMove = zMove;
        this.statHP = statHP;
        this.statSPA = statSPA;
        this.statDEF = statDEF;
        this.statSPD = statSPD;
        this.statSPEED = statSPEED;
    }

    //Pokemon Methods
    public String getPokeName() {
        return pokeName;
    }

    public void setPokeName(String pokeName) {
        this.pokeName = pokeName;
    }

    public int getPokeGrade() {
        return pokeGrade;
    }

    public void setPokeGrade(int pokeGrade) {
        this.pokeGrade = pokeGrade;
    }

    public String getMoveType() {
        return moveType;
    }

    public void setMoveType(String moveType) {
        this.moveType = moveType;
    }

    public int getPokePE() {
        return pokePE;
    }

    public void setPokePE(int pokePE) {
        this.pokePE = pokePE;
    }

    public String getZMove() {
        return zMove;
    }

    public void setZMove(String zMove) {
        this.zMove = zMove;
    }

    public int getStatHP() {
        return statHP;
    }

    public void setStatHP(int statHP) {
        this.statHP = statHP;
    }

    public int getStatSPA() {
        return statSPA;
    }

    public void setStatSPA(int statSPA) {
        this.statSPA = statSPA;
    }

    public int getStatDEF() {
        return statDEF;
    }

    public void setStatDEF(int statDEF) {
        this.statDEF = statDEF;
    }

    public int getStatSPD() {
        return statSPD;
    }

    public void setStatSPD(int statSPD) {
        this.statSPD = statSPD;
    }

    public int getStatSPEED() {
        return statSPEED;
    }

    public void setStatSPEED(int statSPEED) {
        this.statSPEED = statSPEED;
    }

    public void takeDamage(int damage) {
        //TODO: Implement Damage Reduction
        this.statHP -= damage;

        if (this.statHP < 0) {
            this.statHP = 0;
        }else{
            System.out.println(this.pokeName + " has " + this.statHP + " HP remaining.");
        }
    }

    //toString Method
    public String toString() {
        return "Pokemon" + '\n' + "{" + '\n' +
                "Name: " + pokeName + '\n' +
                "Grade: " + pokeGrade + '\n' +
                "moveType: " + moveType + '\n' +
                "PE: " + pokePE + '\n' +
                "zMove: " + zMove + '\n' +
                "HP: " + statHP + '\n' +
                "SP.A: " + statSPA + '\n' +
                "DEF: " + statDEF + '\n' +
                "SP.D: " + statSPD + '\n' +
                "SPEED: " + statSPEED + '\n' +
                '}';
    }

}
