public class Player {

    //Player Attributes
    private String playerName;
    private int battleScores[];

    //Player Constructor
    public Player(String playerName, int[] battleScores) {
        this.playerName = playerName;
        this.battleScores = battleScores;
    }

    //Player Methods
    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int[] getBattleScores() {
        return battleScores;
    }

    public void setBattleScores(int[] battleScores) {
        this.battleScores = battleScores;
    }

    public int getAverageScore() {
        int total = 0;
        for (int i = 0; i < battleScores.length; i++) {
            total += battleScores[i];
        }
        return total / battleScores.length;
    }

    public int getHighestScore() {
        int highest = 0;
        for (int i = 0; i < battleScores.length; i++) {
            if (battleScores[i] > highest) {
                highest = battleScores[i];
            }
        }
        return highest;
    }

    public int getLowestScore() {
        int lowest = battleScores[0];
        for (int i = 0; i < battleScores.length; i++) {
            if (battleScores[i] < lowest) {
                lowest = battleScores[i];
            }
        }
        return lowest;
    }

    public String toString() {
        return "Player Name: " + playerName + "\n" +
                "Player Average Score: " + getAverageScore() + "\n" +
                "Player Highest Score: " + getHighestScore() + "\n" +
                "Player Lowest Score: " + getLowestScore() + "\n";
    }
}
