public class PokeBall {

    //PokeBall Attributes
    private int catchRate;
    private int catchBonus;

    //PokeBall Constructor
    public PokeBall(int catchRate, int catchBonus){
        this.catchRate = catchRate;
        this.catchBonus = catchBonus;
    }

    //PokeBall Methods
    public int getCatchRate(){
        return catchRate;
    }

    public void setCatchRate(int catchRate){
        this.catchRate = catchRate;
    }

    public int getCatchBonus(){
        return catchBonus;
    }

    public void setCatchBonus(int catchBonus){
        this.catchBonus = catchBonus;
    }

    //toString Method
    public String toString(){
        return "Catch Rate: " + catchRate + "\nCatch Bonus: " + catchBonus;
    }
}
