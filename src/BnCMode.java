public class BnCMode {

    //BnCMode Attributes
    private String mode;

    //BnCMode Constructor
    public BnCMode(String mode){
        this.mode = mode;
    }

    //BnCMode Methods
    public String getMode(){
        return mode;
    }

    public void setMode(String mode){
        this.mode = mode;
    }

    public void BattleNCatch(Attacker attacker, Defender defender){
        //Implement BattleNCatch Method (Not sure how to implement this)
    }


    //toString Method
    public String toString(){
        return "Mode: " + mode;
    }

}
