public class GreatBall extends PokeBall {

    //GreatBall Attributes
    private int greatChance;

    //GreatBall Constructor
    public GreatBall(int catchRate, int catchBonus, int greatChance){
        super(catchRate, catchBonus);
        this.greatChance = greatChance;
    }

    //GreatBall Methods
    public int getGreatChance(){
        return greatChance;
    }

    public void setGreatChance(int greatChance){
        this.greatChance = greatChance;
    }

    //toString Method
    public String toString(){
        return super.toString() + "\nGreat Chance: " + greatChance;
    }
}
