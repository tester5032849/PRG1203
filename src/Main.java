import java.util.*;

public class Main {

    public static void main(String[] args){
   
        //Main Attributes
        Scanner myInput = new Scanner(System.in);
        String playerName;
        int mainMenuChoice;
        int catchChoice;
        int battleChoice;
        int pokemonMenuChoice;
        int opponentMenuChoice;


        //Random Stat Generator with values of 1 to 5
        Random rand = new Random();
        int rgStat = rand.nextInt(5) + 1;
        int rpeStat = rand.nextInt(1000) + 1;
        int rpStat = rand.nextInt(50) + 1;


        //Random Pokemon Name Generator
        String[] pokemonNames1 = {"Bulbasaur", "Charmander", "Squirtle"};
        String rpName1 = pokemonNames1[new Random().nextInt(pokemonNames1.length)];

        String[] pokemonNames2 = {"Ivysaur", "Charmeleon", "Wartortle"};
        String rpName2 = pokemonNames2[new Random().nextInt(pokemonNames2.length)];

        String[] pokemonNames3 = {"Venusaur", "Charizard", "Blastoise"};
        String rpName3 = pokemonNames3[new Random().nextInt(pokemonNames3.length)];

        //Random Pokemon Type Generator
        String[] pokemonTypes1 = {"Grass", "Fire", "Water"};
        String rpType1 = pokemonTypes1[new Random().nextInt(pokemonTypes1.length)];

        String[] pokemonTypes2 = {"Grass", "Fire", "Water"};
        String rpType2 = pokemonTypes2[new Random().nextInt(pokemonTypes2.length)];

        String[] pokemonTypes3 = {"Grass", "Fire", "Water"};
        String rpType3 = pokemonTypes3[new Random().nextInt(pokemonTypes3.length)];

        //Random Pokemon Move Generator
        String[] pokemonMoves1 = {"Tackle", "Vine Whip", "Scratch", "Ember", "Water Gun"};
        String rpMove1 = pokemonMoves1[new Random().nextInt(pokemonMoves1.length)];

        String[] pokemonMoves2 = {"Razor Leaf", "Flamethrower", "Hydro Pump"};
        String rpMove2 = pokemonMoves2[new Random().nextInt(pokemonMoves2.length)];

        String[] pokemonMoves3 = {"Solar Beam", "Inferno Overdrive", "Hydro Vortex"};
        String rpMove3 = pokemonMoves3[new Random().nextInt(pokemonMoves3.length)];

        //Random Pokemon zMove Generator
        String[] pokemonzMoves1 = {"Solar Beam", "Inferno Overdrive", "Hydro Vortex"};
        String rpzMove1 = pokemonzMoves1[new Random().nextInt(pokemonzMoves1.length)];

        String[] pokemonzMoves2 = {"Solar Beam", "Inferno Overdrive", "Hydro Vortex"};
        String rpzMove2 = pokemonzMoves2[new Random().nextInt(pokemonzMoves2.length)];

        String[] pokemonzMoves3 = {"Solar Beam", "Inferno Overdrive", "Hydro Vortex"};
        String rpzMove3 = pokemonzMoves3[new Random().nextInt(pokemonzMoves3.length)];



        
        ArrayList<Attacker> Attackers = new ArrayList<Attacker>();
        ArrayList<Defender> Defenders = new ArrayList<Defender>();


        //Pokemon Attributes
        Attacker pokemonA = new Attacker(rpName1, rgStat, rpMove1, rpeStat,
                rpzMove1, rpStat,rpStat,rpStat,rpStat,rpStat,rpType1);
        Attacker pokemon1 = new Attacker(rpName1, rgStat, rpMove1, rpeStat,
                rpzMove1, rpStat,rpStat,rpStat,rpStat,rpStat,rpType1);
        Attacker pokemon2 = new Attacker(rpName2, rgStat, rpMove2, rpeStat,
                rpzMove2, rpStat,rpStat,rpStat,rpStat,rpStat,rpType2);
        Attacker pokemon3 = new Attacker(rpName3, rgStat, rpMove3, rpeStat, 
                rpzMove3, rpStat,rpStat,rpStat,rpStat,rpStat,rpType3);

        Defender pokemonD = new Defender(rpName1, rgStat, rpMove1, rpeStat, 
                rpzMove1, rpStat,rpStat,rpStat,rpStat,rpStat,rpType1);
        Defender pokemon4 = new Defender(rpName1, rgStat, rpMove1, rpeStat, 
                rpzMove1, rpStat,rpStat,rpStat,rpStat,rpStat,rpType1);
        Defender pokemon5 = new Defender(rpName2, rgStat, rpMove2, rpeStat, 
                rpzMove2, rpStat,rpStat,rpStat,rpStat,rpStat,rpType2);
        Defender pokemon6 = new Defender(rpName3, rgStat, rpMove3, rpeStat, 
                rpzMove3, rpStat,rpStat,rpStat,rpStat,rpStat,rpType3);


        Attackers.add(pokemonA);
        Attackers.add(pokemon1);
        Attackers.add(pokemon2);
        Attackers.add(pokemon3);

        Defenders.add(pokemonD);
        Defenders.add(pokemon4);
        Defenders.add(pokemon5);
        Defenders.add(pokemon6);


        //Main Menu Do While Loop

        do {
            mainMenuChoice = 0;
            System.out.println("==========================================");
            System.out.println("Please select an option: ");
            System.out.println("1. Start Game");
            System.out.println("2. Exit");
            mainMenuChoice = myInput.nextInt();
            switch (mainMenuChoice) {
                case 1:
                    System.out.println("Starting Game...");
                    //ASCII Art for Pokemon
                    System.out.println("==========================================");
                    System.out.println("Welcome to Pokemon Battle Simulator!");
                    System.out.println("Made by: Chew Zhan Hong (23018039) and Javen Yong Ye Jo (220097679)");
                    System.out.println("==========================================");
                    System.out.println("Please enter your name: ");
                    playerName = myInput.next();
                    System.out.println("==========================================");
                    System.out.println("Welcome " + playerName + "!");
                    System.out.println("Here are some items to help you on your journey!");
                    System.out.println("PokeBall x 5");
                    System.out.println("GreatBall x 5");
                    System.out.println("UltraBall x 5");
                    //Add a line break
                    System.out.println("==========================================");
                    break;
                case 2:
                    System.out.println("Exiting Game...");
                    break;
                default:
                    System.out.println("Invalid Choice");
                    break;
            }

            if(mainMenuChoice == 1){
                pokemonMenuChoice = 0;
                System.out.println("==========================================");
                System.out.println("Please select your Pokemon: ");
                System.out.printf("1. %s \n", pokemonA.getPokeName());
                System.out.printf("2. %s \n", pokemonA.getPokeName());
                System.out.printf("3. %s \n", pokemonA.getPokeName());
                System.out.println("==========================================");
                pokemonMenuChoice = myInput.nextInt();

                while(pokemonMenuChoice != 1 && pokemonMenuChoice != 2 && pokemonMenuChoice != 3){
                    System.out.println("Invalid Pokemon Choice");
                    System.out.println("==========================================");
                    System.out.println("Please select your Pokemon: ");
                    System.out.printf("1. %s \n", pokemonA.getPokeName());
                    System.out.printf("2. %s \n", pokemonA.getPokeName());
                    System.out.printf("3. %s \n", pokemonA.getPokeName());
                    System.out.println("==========================================");
                    pokemonMenuChoice = myInput.nextInt();
                }
                //Pokemon Menu Switch Case
                switch (pokemonMenuChoice) {
                    case 1:
                        System.out.println(pokemonA.toString());
                        System.out.println("==========================================");
                        System.out.printf("You have chosen %s as your Pokemon!\n", pokemonA.getPokeName());
                        System.out.println("==========================================");
                        break;
                    case 2:
                        System.out.println(pokemonA.toString());
                        System.out.println("==========================================");
                        System.out.printf("You have chosen %s as your Pokemon!\n", pokemonA.getPokeName());
                        System.out.println("==========================================");
                        break;
                    case 3:
                        System.out.println(pokemonA.toString());
                        System.out.println("==========================================");
                        System.out.printf("You have chosen %s as your Pokemon!\n", pokemonA.getPokeName());
                        System.out.println("==========================================");
                        break;
                    default:
                        System.out.println("Invalid Pokemon Choice\n");
                        break;
                }

                if(pokemonMenuChoice == 1 || pokemonMenuChoice == 2 || pokemonMenuChoice == 3){
                    opponentMenuChoice = 0;
                    System.out.println("==========================================");
                    System.out.println("Please select your opponent: ");
                    System.out.printf("1. %s \n", pokemonD.getPokeName());
                    System.out.printf("2. %s \n", pokemonD.getPokeName());
                    System.out.printf("3. %s \n", pokemonD.getPokeName());
                    System.out.println("==========================================");
                    opponentMenuChoice = myInput.nextInt();

                    while(opponentMenuChoice != 1 && opponentMenuChoice != 2 && opponentMenuChoice != 3){
                        System.out.println("Invalid Opponent Choice");
                        System.out.println("==========================================");
                        System.out.println("Please select your opponent: ");
                        System.out.printf("1. %s \n", pokemonD.getPokeName());
                        System.out.printf("2. %s \n", pokemonD.getPokeName());
                        System.out.printf("3. %s \n", pokemonD.getPokeName());
                        System.out.println("==========================================");
                        opponentMenuChoice = myInput.nextInt();
                    }

                    //Opponent Menu Switch Case
                    switch (opponentMenuChoice) {
                        case 1:
                            System.out.println(pokemonD.toString());
                            System.out.println("==========================================");
                            System.out.printf("You have chosen %s as your opponent!\n", pokemonD.getPokeName());
                            System.out.println("==========================================");
                            break;
                        case 2:
                            System.out.println(pokemonD.toString());
                            System.out.println("==========================================");
                            System.out.printf("You have chosen %s as your opponent!\n", pokemon5.getPokeName());
                            System.out.println("==========================================");
                            break;
                        case 3:
                            System.out.println(pokemonD.toString());
                            System.out.println("==========================================");
                            System.out.printf("You have chosen %s as your opponent!\n", pokemonD.getPokeName());
                            System.out.println("==========================================");
                            break;
                        default:
                            System.out.println("Invalid Opponent Choice\n");
                            break;
                    }

                    if(opponentMenuChoice == 1 || opponentMenuChoice == 2 || opponentMenuChoice == 3){
                        battleChoice = 0;
                        System.out.println("==========================================");
                        System.out.println("Battle Start!");
                        System.out.println("==========================================");
                        System.out.println("==========================================");
                        System.out.println("Please select an option: ");
                        System.out.println("1. Attack");
                        System.out.println("2. Z-Move");
                        System.out.println("==========================================");
                        battleChoice = myInput.nextInt();

                        while(battleChoice != 1 && battleChoice != 2){
                            System.out.println("Invalid Battle Choice");
                            System.out.println("==========================================");
                            System.out.println("Please select an option: ");
                            System.out.println("1. Attack");
                            System.out.println("2. Z-Move");
                            System.out.println("==========================================");
                            battleChoice = myInput.nextInt();
                        }

                    switch(battleChoice){
                        case 1:
                            System.out.println("==========================================");
                            System.out.println("You have chosen to attack!");
                            pokemonD.setStatHP(pokemonD.getStatHP() - rpStat);
                            System.out.printf("%s's HP is now %d\n", pokemonD.getPokeName(), pokemonD.getStatHP());
                            System.out.println("Opponent's turn!");
                            pokemonA.setStatHP(pokemonA.getStatHP() - rpStat);
                            System.out.printf("%s's HP is now %d\n", pokemonA.getPokeName(), pokemonA.getStatHP());
                            System.out.println("==========================================");
                            break;
                        case 2:
                            System.out.println("==========================================");
                            System.out.println("You have chosen to use Z-Move!");
                            pokemonD.setStatHP(pokemonD.getStatHP() - rpStat);
                            System.out.printf("%s's HP is now %d\n", pokemonD.getPokeName(), pokemonD.getStatHP());
                            System.out.println("Opponent's turn!");
                            pokemonA.setStatHP(pokemonA.getStatHP() - rpStat);
                            System.out.printf("%s's HP is now %d\n", pokemonA.getPokeName(), pokemonA.getStatHP());
                            System.out.println("==========================================");
                            break;
                        default:
                            System.out.println("Invalid Battle Choice");
                            break;
                    }

                        if(battleChoice == 1 || battleChoice == 2){
                            catchChoice = 0;
                            if(pokemonD.getStatHP() == 0){
                            
                                System.out.println("==========================================");
                                System.out.println("You have defeated the opponent!");
                                System.out.println("You can now catch the Pokemon!");
                                System.out.println("==========================================");
                            }else{
                                System.out.println("==========================================");
                                System.out.println("You have been defeated by the opponent!");
                                System.out.println("==========================================");
                            }

                            System.out.println("==========================================");
                            System.out.println("Please select an option: ");
                            System.out.println("1. PokeBall");
                            System.out.println("2. GreatBall");
                            System.out.println("3. UltraBall");
                            System.out.println("==========================================");
                            catchChoice = myInput.nextInt();

                         while (catchChoice != 1 && catchChoice != 2 && catchChoice != 3) {
                                System.out.println("Invalid Catch Choice");
                                System.out.println("==========================================");
                                System.out.println("Please select an option: ");
                                System.out.println("1. PokeBall");
                                System.out.println("2. GreatBall");
                                System.out.println("3. UltraBall");
                                System.out.println("==========================================");
                                catchChoice = myInput.nextInt();
                        }

                        switch(catchChoice){
                            case 1:
                                System.out.println("==========================================");
                                System.out.println("You have chosen to use PokeBall!");
                                System.out.println("Pokemon has been caught!");
                                System.out.println("==========================================");
                                break;
                            case 2:
                                System.out.println("==========================================");
                                System.out.println("You have chosen to use GreatBall!");
                                System.out.println("Pokemon has been caught!");
                                System.out.println("==========================================");
                                break;
                            case 3:
                                System.out.println("==========================================");
                                System.out.println("You have chosen to use UltraBall!");
                                System.out.println("Pokemon has been caught!");
                                System.out.println("==========================================");
                                break;
                            default:
                                System.out.println("Invalid Catch Choice");
                                break;
                        }

                        if(catchChoice == 1 || catchChoice == 2 || catchChoice == 3){
                            mainMenuChoice = 0;
                            System.out.println("==========================================");
                            System.out.println("Battle End!");
                            System.out.println("==========================================");
                            System.out.println("==========================================");
                            System.out.println("Please select an option: ");
                            System.out.println("1. Continue");
                            System.out.println("2. Exit");
                            System.out.println("==========================================");
                            mainMenuChoice = myInput.nextInt();


                            while (mainMenuChoice != 1 && mainMenuChoice != 2) {
                                System.out.println("Invalid Choice");
                                System.out.println("==========================================");
                                System.out.println("Please select an option: ");
                                System.out.println("1. Continue");
                                System.out.println("2. Exit");
                                System.out.println("==========================================");
                                mainMenuChoice = myInput.nextInt();
                            }

                            switch(mainMenuChoice){
                                case 1:
                                    System.out.println("==========================================");
                                    System.out.println("Continuing Game...");
                                    System.out.println("==========================================");
                                    System.out.println("==========================================");
                                    System.out.println("Please select your Pokemon: ");
                                    System.out.printf("1. %s \n", pokemonA.getPokeName());
                                    System.out.printf("2. %s \n", pokemonA.getPokeName());
                                    System.out.printf("3. %s \n", pokemonA.getPokeName());
                                    System.out.println("==========================================");
                                    pokemonMenuChoice = myInput.nextInt();
                                    break;
                                case 2:
                                    System.out.println("==========================================");
                                    System.out.println("Exiting Game...");
                                    System.out.println("==========================================");
                                    break;
                                default:
                                    System.out.println("Invalid Choice");
                                    break;
                            }

                        }

                 }

            }


            }else{
                break;
                }
            }

        } while (mainMenuChoice != 1 && mainMenuChoice != 2);

        //Maybe add somethings here

    }

}
