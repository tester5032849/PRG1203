public class Defender extends Pokemon {

    //Instance Variables
    private String pokeType;

    //Defender Constructor
    public Defender(String pokeName, int pokeGrade, String moveType, int pokePE,
            String zMove, int statHP, int statSPA, int statDEF, int statSPD, int statSPEED, String pokeType){
    
        super(pokeName, pokeGrade, moveType, pokePE, zMove, statHP, statSPA, statDEF, statSPD, statSPEED);

        this.pokeType = pokeType;
    }

    //Accessor Methods
    public String getPokeType(){
        return pokeType;
    }

    //Mutator Methods
    public void setPokeType(String pokeType){
        this.pokeType = pokeType;
    }

    public void takeDamage(int damage){
        super.takeDamage(damage);
        //Implement Damage Reduction based on Type Advantage & Disadvantage
        if(pokeType.equals("Fire")){
            if(getPokeType().equals("Grass")){
                damage = damage*2;
            }
            else if(getPokeType().equals("Water")){
                damage = damage/2;
            }
        }
        else if(pokeType.equals("Water")){
            if(getPokeType().equals("Fire")){
                damage = damage*2;
            }
            else if(getPokeType().equals("Grass")){
                damage = damage/2;
            }
        }
        else if(pokeType.equals("Grass")){
            if(getPokeType().equals("Water")){
                damage = damage*2;
            }
            else if(getPokeType().equals("Fire")){
                damage = damage/2;
            }
        }
    }

    //toString Method
    public String toString(){
        return super.toString() + "\nDefend Type: " + pokeType;
    }
}
