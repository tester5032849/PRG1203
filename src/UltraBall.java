public class UltraBall extends PokeBall {

    //UltraBall Attributes
    private int ultraChance;

    //UltraBall Constructor
    public UltraBall(int catchRate, int catchBonus, int ultraChance){
        super(catchRate, catchBonus);
        this.ultraChance = ultraChance;
    }

    //UltraBall Methods
    public int getUltraChance(){
        return ultraChance;
    }

    public void setUltraChance(int ultraChance){
        this.ultraChance = ultraChance;
    }

    //toString Method
    public String toString(){
        return super.toString() + "\nUltra Chance: " + ultraChance;
    }
}
